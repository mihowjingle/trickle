# Welcome to _Trickle_

It all started when I needed something, that eventually ended up as (use-wise)...
```java
List<Integer> numbers = Trickle
        .join(asList(1, 2, 3), asList(-1, -2, -3))
        .match((x, y) -> x.equals(-y))
        .exactOrElseThrow(() -> new IllegalStateException("Whoops! Exact one-to-one match expected!"))
        .merge((x, y) -> x * y);

// should produce [-1, -4, -9]
```
...at work, again (of course not for some numbers).
Then I thought that maybe I should make a library (or at least could, because why not).
I added tuples and other convenience stuff. Maybe I'll finish it one day.

The name comes from how data _trickles_ down the fluent API calls.