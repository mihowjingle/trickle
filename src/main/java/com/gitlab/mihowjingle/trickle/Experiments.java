package com.gitlab.mihowjingle.trickle;

import static java.util.Arrays.asList;

public class Experiments {
    public static void main(String[] args) {

        // todo make actual test of all these (and then some)

        Trickle.join(asList(1, 2, 3), asList(-3, -4, -5))
                .cartesian()
                .forEach(couple -> System.out.println(couple.getFirst() + " * " + couple.getSecond() + " = " + couple.getFirst() * couple.getSecond()));

        System.out.println("=-=-=-=-=-=-=-=-=-=");

        boolean any = Trickle
                .join(asList(1, 2, 3), asList(-3, -4, -5))
                .occurrencesOf((x, y) -> x.equals(y + 4))
                .any();
        System.out.println("Is any number from the first list bigger by exactly 4 than any number from the second list? " + any);

        System.out.println("=-=-=-=-=-=-=-=-=-=");

        boolean all = Trickle
                .join(asList(1, 2, 3), asList(-3, -4, -5))
                .occurrencesOf((x, y) -> x >= y + 1)
                .all();
        System.out.println("Are all numbers from the first list bigger by at least 1 than all numbers from the second list? " + all);

        System.out.println("=-=-=-=-=-=-=-=-=-=");

        boolean none = Trickle
                .join(asList(1, 2, 3), asList(-3, -4, -5))
                .occurrencesOf((x, y) -> x >= y)
                .none();
        System.out.println("Are no numbers from the first list greater or equal to at least one number from the second list? " + none);

        System.out.println("=-=-=-=-=-=-=-=-=-=");

        boolean exactlyTwo = Trickle
                .join(asList(1, 2, 3), asList(-3, -4, -5))
                .occurrencesOf((x, y) -> x >= y)
                .exactly(2);
        System.out.println("Are there exactly 2 pairs, " +
                "where the number from the first list is greater or equal to the number from the second list? " + exactlyTwo);

        System.out.println("=-=-=-=-=-=-=-=-=-=");

        int count = Trickle
                .join(asList(1, 2, 3), asList(-3, -4, -5))
                .occurrencesOf((x, y) -> x > 0 && y > 0)
                .count();
        System.out.println("How many pairs where both numbers are positive? " + count);

        System.out.println("=-=-=-=-=-=-=-=-=-=");

        Trickle
                .join(asList(1, 2, 3), asList(-3, -4, -5))
                .match((x, y) -> x.equals(-y))
                .lenient()
                .merge((x, y) -> x * y)
                .forEach(System.out::println);

        System.out.println("=-=-=-=-=-=-=-=-=-=");

        Trickle // exception intentional
                .join(asList(1, 2, 3), asList(-3, -4, -5))
                .match((x, y) -> x.equals(-y))
                .exactOrElseThrow(() -> new IllegalStateException("Whoops! Exact one-to-one match expected!"))
                .merge((x, y) -> x * y)
                .forEach(System.out::println);
    }
}
