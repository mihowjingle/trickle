package com.gitlab.mihowjingle.trickle;

import com.gitlab.mihowjingle.trickle.connectors.bi.BiMatcher;

import javax.annotation.Nonnull;

/**
 * The "root" of the library.
 */
public final class Trickle {

    private Trickle() {}

    public static <T, U> BiMatcher<T, U> join(@Nonnull Iterable<T> t, @Nonnull Iterable<U> u) {
        return BiMatcher.join(t, u);
    }

    // todo @NotNull and requireNonNull() everywhere (or @Nullable), or add some descriptive error message and @throws, where there already is requireNonNull()
    // todo javadoc everywhere
}
