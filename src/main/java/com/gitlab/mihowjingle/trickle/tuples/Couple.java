package com.gitlab.mihowjingle.trickle.tuples;

import javax.annotation.Nonnull;
import java.util.Objects;

public final class Couple<T, U> {
    private final T first;
    private final U second;

    private Couple(T first, U second) {
        this.first = first;
        this.second = second;
    }

    @Nonnull
    public static <T, U> Couple<T, U> of(T first, U second) {
        return new Couple<>(first, second);
    }

    public T getFirst() {
        return first;
    }

    public U getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Couple<?, ?> couple = (Couple<?, ?>) other;
        return first.equals(couple.first) && second.equals(couple.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    @Override
    public String toString() {
        return "Couple{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
