package com.gitlab.mihowjingle.trickle.connectors.bi;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class ExactBiMerger<T, U> implements BiMerger<T, U> {
    private final Iterable<T> first;
    private final Iterable<U> second;
    private final BiPredicate<T, U> predicate;
    private final Supplier<RuntimeException> supplier;

    ExactBiMerger(Iterable<T> first, Iterable<U> second, BiPredicate<T, U> predicate, Supplier<RuntimeException> supplier) {
        this.first = first;
        this.second = second;
        this.predicate = predicate;
        this.supplier = supplier;
    }

    @Nonnull
    @Override
    public <R> List<R> merge(@Nonnull BiFunction<T, U, R> function) {
        requireNonNull(function);
        List<R> result = new LinkedList<>();
        for (T t : first) {
            U u = findExactlyOneMatchOrThrow(t);
            result.add(function.apply(t, u));
        }
        return result;
    }

    @Override
    public void forEach(BiConsumer<T, U> consumer) {
        // todo
    }

    // todo so exactly one one the right has to match (given) one on the left
    //  what about exactly one on the left matching (given) one on the right?
    //  test!
    private U findExactlyOneMatchOrThrow(T t) {
        U firstFound = null;
        for (U u : second) {
            if (predicate.test(t, u)) {
                if (firstFound != null) {
                    throw supplier.get();
                }
                firstFound = u;
            }
        }
        if (firstFound != null) {
            return firstFound;
        }
        throw supplier.get();
    }
}
