package com.gitlab.mihowjingle.trickle.connectors.bi;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public interface BiMerger<T, U> {
    <R> List<R> merge(BiFunction<T, U, R> function);
    void forEach(BiConsumer<T, U> consumer);    //  + one eliminated step, mimicking destructuring operation
                                                //  - ...? duplication (of forEach, conceptually speaking)?
}
