package com.gitlab.mihowjingle.trickle.connectors.bi;

import java.util.function.BiPredicate;

public class BiCounter<T, U> {
    private final Iterable<T> first;
    private final Iterable<U> second;
    private final BiPredicate<T, U> predicate;

    BiCounter(Iterable<T> first, Iterable<U> second, BiPredicate<T, U> predicate) {
        this.first = first;
        this.second = second;
        this.predicate = predicate;
    }

    public int count() {
        int count = 0;
        for (T t : first) {
            for (U u : second) {
                if (predicate.test(t, u)) {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean any() {
        for (T t : first) {
            for (U u : second) {
                if (predicate.test(t, u)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean all() {
        for (T t : first) {
            for (U u : second) {
                if (!predicate.test(t, u)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean none() {
        for (T t : first) {
            for (U u : second) {
                if (predicate.test(t, u)) {
                    return false;
                }
            }
        }
        return true;
    }

    // mostly syntactic sugar for .count(predicate) == occurrences (unless... performance? probably negligible, todo test)
    public boolean exactly(int occurrences) {
        int count = 0;
        for (T t : first) {
            for (U u : second) {
                if (predicate.test(t, u)) {
                    count++;
                    if (count > occurrences) {
                        return false;
                    }
                }
            }
        }
        return count == occurrences;
    }

    // todo other variations:
    //  atLeast(occurrences),
    //  atMost(occurrences),
    //  fewerThan(occurrences),
    //  moreThan(occurrences),
    //  allBut(occurrences), etc... ?
    //  notAll()?
}
