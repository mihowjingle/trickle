package com.gitlab.mihowjingle.trickle.connectors.bi;

import com.gitlab.mihowjingle.trickle.tuples.Couple;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;

import static java.util.Objects.requireNonNull;

public class BiMatcher<T, U> {
    private final Iterable<T> first;
    private final Iterable<U> second;

    private BiMatcher(Iterable<T> first, Iterable<U> second) {
        this.first = first;
        this.second = second;
    }

    @Nonnull
    public static <T, U> BiMatcher<T, U> join(@Nonnull Iterable<T> t, @Nonnull Iterable<U> u) {
        requireNonNull(t);
        requireNonNull(u);
        return new BiMatcher<>(t, u);
    }

    @Nonnull
    public BiMergingStrategy<T, U> match(@Nonnull BiPredicate<T, U> predicate) {
        requireNonNull(predicate);
        return new BiMergingStrategy<>(first, second, predicate);
    }

    @Nonnull
    public BiCounter<T, U> occurrencesOf(@Nonnull BiPredicate<T, U> predicate) {
        requireNonNull(predicate);
        return new BiCounter<>(first, second, predicate);
    }

    // todo
    //  ? move to Trickle,
    //  ? make static,
    //  ? add first and second as arguments,
    //  ! add overload with Iterable -> Collection
    //  ! would know the initial capacity... could use an array-list... worth it? investigate
    @Nonnull
    public List<Couple<T, U>> cartesian() {
        List<Couple<T, U>> result = new LinkedList<>();
        for (T t : first) {
            for (U u : second) {
                result.add(Couple.of(t, u));
            }
        }
        return result;
    }
}
