package com.gitlab.mihowjingle.trickle.connectors.bi;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import static java.util.Objects.requireNonNull;

public class LenientBiMerger<T, U> implements BiMerger<T, U> {
    private final Iterable<T> first;
    private final Iterable<U> second;
    private final BiPredicate<T, U> predicate;

    LenientBiMerger(Iterable<T> first, Iterable<U> second, BiPredicate<T, U> predicate) {
        this.first = first;
        this.second = second;
        this.predicate = predicate;
    }

    @Nonnull
    @Override
    public <R> List<R> merge(@Nonnull BiFunction<T, U, R> function) {
        requireNonNull(function);
        List<R> result = new LinkedList<>();
        for (T t : first) {
            for (U u : second) {
                if (predicate.test(t, u)) {
                    result.add(function.apply(t, u));
                }
            }
        }
        return result;
    }

    @Override
    public void forEach(BiConsumer<T, U> consumer) {
        // todo
    }
}
