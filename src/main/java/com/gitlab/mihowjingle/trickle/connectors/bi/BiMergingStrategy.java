package com.gitlab.mihowjingle.trickle.connectors.bi;

import javax.annotation.Nonnull;
import java.util.function.BiPredicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class BiMergingStrategy<T, U> { // todo BiMergingStrategySelector, or something? ...or something else entirely? eh, maybe not, but we'll see
    private final Iterable<T> first;
    private final Iterable<U> second;
    private final BiPredicate<T, U> predicate;

    BiMergingStrategy(Iterable<T> first, Iterable<U> second, BiPredicate<T, U> predicate) {
        this.first = first;
        this.second = second;
        this.predicate = predicate;
    }

    @Nonnull
    public BiMerger<T, U> exactOrElseThrow(@Nonnull Supplier<RuntimeException> supplier) {
        requireNonNull(supplier);
        return new ExactBiMerger<>(first, second, predicate, supplier);
    }

    @Nonnull
    public BiMerger<T, U> lenient() { // todo @NotNull
        return new LenientBiMerger<>(first, second, predicate);
    }
}
