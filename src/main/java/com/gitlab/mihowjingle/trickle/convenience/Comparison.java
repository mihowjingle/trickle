package com.gitlab.mihowjingle.trickle.convenience;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Some "smooth", semantic syntactic sugar for comparing Comparables,
 * intended to be more intuitive and readable than comparing the result of a comparison to zero.
 */
public final class Comparison { // todo also copy the tests from lodgings

    private Comparison() {}

    public enum Operator {
        EQUAL_TO {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) == 0;
            }
        },
        NOT_EQUAL_TO {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) != 0;
            }
        },
        GREATER_THAN {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) > 0;
            }
        },
        GREATER_THAN_OR_EQUAL_TO {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) >= 0;
            }
        },
        LESS_THAN {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) < 0;
            }
        },
        LESS_THAN_OR_EQUAL_TO {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) <= 0;
            }
        };

        abstract <T extends Comparable<T>> boolean apply(T left, T right);
    }

    /**
     * Some "smooth", semantic syntactic sugar for comparing Comparables,
     * intended to be more intuitive and readable than comparing the result of a comparison to zero.
     *
     * @throws NullPointerException when any of the arguments is null
     */
    public static <T extends Comparable<T>> boolean is(@Nonnull T left, @Nonnull Operator operator, @Nonnull T right) {
        requireNonNull(left, "left operand is null");
        requireNonNull(right, "right operand is null");
        requireNonNull(operator, "operator is null");
        return operator.apply(left, right);
    }
}
