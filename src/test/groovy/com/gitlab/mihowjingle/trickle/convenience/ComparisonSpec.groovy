package com.gitlab.mihowjingle.trickle.convenience

import spock.lang.Specification

import static com.gitlab.mihowjingle.trickle.convenience.Comparison.Operator.*
import static com.gitlab.mihowjingle.trickle.convenience.Comparison.is

class ComparisonSpec extends Specification {

    def "it should compare things correctly"() {

        expect:
        is(left, EQUAL_TO, right) == equal
        is(left, NOT_EQUAL_TO, right) == !equal
        is(left, GREATER_THAN, right) == leftGreater
        is(left, GREATER_THAN_OR_EQUAL_TO, right) == leftGE
        is(left, LESS_THAN, right) == leftLess
        is(left, LESS_THAN_OR_EQUAL_TO, right) == leftLE

        where:
        left    | right     || equal    | leftGreater   | leftGE    | leftLess  | leftLE
        1       | 0         || false    | true          | true      | false     | false
        1       | 1         || true     | false         | true      | false     | true
        1       | 2         || false    | false         | false     | true      | true
        2       | 3         || false    | false         | false     | true      | true
        3       | 3         || true     | false         | true      | false     | true
        4       | 3         || false    | true          | true      | false     | false
    }
}
